//
//  indexedImage.swift
//  swiftIndexedImages
//
//  Created by tom Hastik on 24/12/2018.
//  Copyleft (ɔ) 2018年 ha100. All rights reversed.
//

import UIKit
import PlaygroundSupport

var swift: Array<UInt8> = [ 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 0, 0,
                            0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,
                            1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2,
                            1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                            1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1,
                            1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1,
                            1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1,
                            1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1,
                            1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1,
                            1, 1, 1, 1, 1, 1, 1, 0, 0, 2, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1,
                            1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1,
                            1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1,
                            1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1,
                            1, 1, 1, 1, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 2, 1, 1, 1,
                            1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1,
                            1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1,
                            1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                            2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2,
                            0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,
                            0, 0, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 0, 0 ]

final class SwiftController: UIViewController {

    let width = 20
    let height = 20

    override func loadView() {

        self.view = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        self.view.backgroundColor = .white

        let view = UIImageView(frame: self.view.frame.insetBy(dx: 25, dy: 25))
        view.backgroundColor = .white
        view.contentMode = .center
        view.image = self.imageFromArray(swift, width: width, height: height)

        self.view.addSubview(view)
    }

    func imageFromArray(_ pixels: Array<UInt8>, width: Int, height: Int) -> UIImage? {

        let dataProviderReleaseCallback: CGDataProviderReleaseDataCallback = { (info: UnsafeMutableRawPointer?, data: UnsafeRawPointer, size: Int) -> () in
            return
        }

        guard let providerRef = pixels.withUnsafeBufferPointer({ pointer -> CGDataProvider? in

            guard let baseAddress = pointer.baseAddress else { print(">>> 💥 failed on pointerBase"); return nil }

            return CGDataProvider(dataInfo: nil,
                                  data: baseAddress,
                                  size: pixels.count,
                                  releaseData: dataProviderReleaseCallback)

        }) else { print(">>> 💥 failed on provider"); return nil }

        let colorTable: Array<UInt8> = [ 255, 255, 255,
                                         233, 52, 37,
                                         243, 153, 147 ]

        guard let colorSpace = CGColorSpace(indexedBaseSpace: CGColorSpaceCreateDeviceRGB(),
                                            last: colorTable.count - 1,
                                            colorTable: colorTable) else { print(">>> 💥 failed on colorSpace"); return nil }

        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.none.rawValue)

        guard let cgim = CGImage(width: width,
                                 height: height,
                                 bitsPerComponent: 8,
                                 bitsPerPixel: 8,
                                 bytesPerRow: width * Int(MemoryLayout<UInt8>.size),
                                 space: colorSpace,
                                 bitmapInfo: bitmapInfo,
                                 provider: providerRef,
                                 decode: nil,
                                 shouldInterpolate: false,
                                 intent: .defaultIntent) else { print(">>> 💥 failed on image"); return nil }

        return UIImage(cgImage: cgim)
    }
}

PlaygroundPage.current.liveView = SwiftController()
