# ![swift pixel logo](images/swift-pixel-logo.png) swift indexed images

transform collection of UInt8 values into indexed image

it is common for simple indie games with custom engines to load sprites from a collection of values representing pixel colors. color is usually in the ARGB or BGRA format which means that 4 bytes of memory are being utilized for every single pixel. so for sprite sheet consisting of 2048 x 2048 pixels a 16 megabyte chunk of memory would be used. this clearly is not very optimal for a simple pixel art game with limited color palette.
<img align="right" src="images/swiftExplained.png">

the same sprite sheet represented as indexed image would cost roughly 4 megabytes plus the size of the palette which can hold up to 256 colors. add to the equation some dithering, color cycling and palette shifting and some serious magic will happen


as presented by Mark Ferrari at gdc 2016 [8 Bit & '8 Bitish' Graphics-Outside the Box](https://www.youtube.com/watch?v=aMcJ1Jvtef0)

```
0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 0, 0
0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1
1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1
1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1
1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1
1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1
1, 1, 1, 1, 1, 1, 1, 0, 0, 2, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1
1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1
1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1
1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1
1, 1, 1, 1, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 2, 1, 1, 1
1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1
1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2
0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0
0, 0, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 0, 0
```

```swift

func imageFromArray(_ pixels: Array<UInt8>, width: Int, height: Int) -> UIImage? {

  let dataProviderReleaseCallback: CGDataProviderReleaseDataCallback = { (info: UnsafeMutableRawPointer?, data: UnsafeRawPointer, size: Int) -> () in
    return
  }

  guard let providerRef = pixels.withUnsafeBufferPointer({ pointer -> CGDataProvider? in

      guard let baseAddress = pointer.baseAddress else { print(">>> 💥 failed on pointerBase"); return nil }

      return CGDataProvider(dataInfo: nil,
                            data: baseAddress,
                            size: pixels.count,
                            releaseData: dataProviderReleaseCallback)

  }) else { print(">>> 💥 failed on provider"); return nil }

  let colorTable: Array<UInt8> = [ 255, 255, 255,
                                   233, 52, 37,
                                   243, 153, 147 ]

  guard let colorSpace = CGColorSpace(indexedBaseSpace: CGColorSpaceCreateDeviceRGB(),
                                      last: colorTable.count - 1,
                                      colorTable: colorTable) else { print(">>> 💥 failed on colorSpace"); return nil }

  let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.none.rawValue)

  guard let cgim = CGImage(width: width,
                           height: height,
                           bitsPerComponent: 8,
                           bitsPerPixel: 8,
                           bytesPerRow: width * Int(MemoryLayout<UInt8>.size),
                           space: colorSpace,
                           bitmapInfo: bitmapInfo,
                           provider: providerRef,
                           decode: nil,
                           shouldInterpolate: false,
                           intent: .defaultIntent) else { print(">>> 💥 failed on image"); return nil }

  return UIImage(cgImage: cgim)
}
```
